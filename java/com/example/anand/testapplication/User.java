package com.example.anand.testapplication;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

public class User implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("company")
    @Expose
    private Company company;

    public User(int id, String name, String username, String email, Address address, String phone, String website, Company company) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.website = website;
        this.company = company;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }


    /*Comparator for sorting the list by User Name (Ascending)*/
    public static Comparator<User> UserNameComparatorAsc = new Comparator<User>() {

        public int compare(User s1, User s2) {
            String userName1 = s1.getName().toUpperCase();
            String userName2 = s2.getName().toUpperCase();

            //ascending order
            return userName1.compareTo(userName2);
        }};

    /*Comparator for sorting the list by User Name (Descending)*/
    public static Comparator<User> UserNameComparatorDesc = new Comparator<User>() {

        public int compare(User s1, User s2) {
            String userName1 = s1.getName().toUpperCase();
            String userName2 = s2.getName().toUpperCase();

             //descending order
            return userName2.compareTo(userName1);
        }};

    @Override
    public String toString() {
        return "[ id=" + id + ", name=" + name + ", email=" + email + "]";
    }

}
