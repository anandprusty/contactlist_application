package com.example.anand.testapplication;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecyclerFragment extends Fragment implements SearchView.OnQueryTextListener {

    private static final String TAG = MainActivity.class.getName();
    List<User> mUserModel;
    private View view;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private RequestQueue mRequestQueue;
    private JsonArrayRequest mJsonRequest;
    private String mUrl = "http://jsonplaceholder.typicode.com/users";
    private TextView mLoadingText;

    private UserDetailsRecyclerViewAdapter mUserRecyclerViewAdapter;

    private Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.recycler_fragment_layout, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mLoadingText = (TextView) view.findViewById(R.id.loading_text_textView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);


        mUserModel = new ArrayList<>();

        getUserDetailsResponseFromServer();

        mUserRecyclerViewAdapter = new UserDetailsRecyclerViewAdapter(mUserModel, mContext);
        mRecyclerView.setAdapter(mUserRecyclerViewAdapter);

        return view;
    }

    private void getUserDetailsResponseFromServer() {

        //RequestQueue initialized
        mRequestQueue = Volley.newRequestQueue(mContext);

        //String Request initialized
        mJsonRequest = new JsonArrayRequest(mUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    // Parsing json array response
                    // loop through each json object
                    for (int i = 0; i < response.length(); i++) {

                        JSONObject user = (JSONObject) response
                                .get(i);

                        String id = user.getString("id");
                        String name = user.getString("name");
                        String username = user.getString("username");
                        String email = user.getString("email");
                        String phone = user.getString("phone");
                        String website = user.getString("website");

                        //For Address
                        JSONObject address = user
                                .getJSONObject("address");
                        String street = address.getString("street");
                        String suite = address.getString("suite");
                        String city = address.getString("city");
                        String zipcode = address.getString("zipcode");

                        //For Geo
                        JSONObject geo = address
                                .getJSONObject("geo");
                        String lat = geo.getString("lat");
                        String lng = geo.getString("lng");

                        //For Company
                        JSONObject company = user
                                .getJSONObject("company");
                        String comp_name = company.getString("name");
                        String catchPhrase = company.getString("catchPhrase");
                        String bs = company.getString("bs");

                        mUserModel.add(new User(Integer.parseInt(id), name, username, email, new Address(street, suite, city, zipcode, new Geo(lat, lng)), phone, website, new Company(comp_name, catchPhrase, bs)));
                    }

                    mUserRecyclerViewAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext,
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }

                mProgressBar.setVisibility(View.GONE);
                mLoadingText.setVisibility(View.GONE);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(mContext,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        mRequestQueue.add(mJsonRequest);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu, menu);

        final MenuItem sortAscItem = menu.findItem(R.id.menu_ascending);
        final MenuItem sortDscItem = menu.findItem(R.id.menu_descending);
        final MenuItem searchItem = menu.findItem(R.id.menu_search);

        sortAscItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                mUserModel = sortAscending();
                mUserRecyclerViewAdapter.notifyDataSetChanged();
                return true;
            }
        });


        sortDscItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                mUserModel = sortDescending();
                mUserRecyclerViewAdapter.notifyDataSetChanged();
                return true;
            }
        });

        menu.findItem(R.id.menu_delete).setVisible(false);


        final SearchView searchView = (SearchView) searchItem.getActionView();
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(Color.parseColor("#FFFFFF"));
        searchView.setOnQueryTextListener(this);

    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        final List<User> filteredModelList = filter(mUserModel, newText);

        mUserRecyclerViewAdapter.setFilter(filteredModelList);

        return true;
    }

    private List<User> filter(List<User> models, String query) {
        query = query.toLowerCase();
        final List<User> filteredModelList = new ArrayList<>();
        for (User model : models) {
            final String text = model.getName().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    public List<User> sortAscending() {

        Collections.sort(mUserModel, User.UserNameComparatorAsc);

        return mUserModel;
    }

    public List<User> sortDescending() {

        Collections.sort(mUserModel, User.UserNameComparatorDesc);

        return mUserModel;
    }

}
