package com.example.anand.testapplication;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class UserDetailsRecyclerViewAdapter extends RecyclerView.Adapter<UserDetailsRecyclerViewAdapter.ViewHolder> {
    private List<User> userList;
    private Context context;


    public UserDetailsRecyclerViewAdapter(List<User> cLst, Context ctx) {
        userList = cLst;
        context = ctx;
    }

    @Override
    public UserDetailsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row, parent, false);

        UserDetailsRecyclerViewAdapter.ViewHolder viewHolder = new UserDetailsRecyclerViewAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(UserDetailsRecyclerViewAdapter.ViewHolder holder, final int position) {


        User user = userList.get(position);
        int id = user.getId();
        holder.mId.setText(id + "");
        String name = user.getName();
        String[] parts = name.split(" ");
        holder.mFirstName.setText(parts[0]);
        holder.mLastName.setText(parts[1]);
        holder.mEmail.setText(user.getEmail());
        holder.mUserName.setText(user.getUsername());
        holder.mPhone.setText(user.getPhone());
        holder.mAddress.setText(user.getAddress().getStreet() + " " + user.getAddress().getSuite() + " " + user.getAddress().getCity() + " " + user.getAddress().getZipcode());
        holder.mWebsite.setText(user.getWebsite());
        holder.mCompany.setText(user.getCompany().getName() + " " + user.getCompany().getCatchPhrase() + " " + user.getCompany().getBs());
        holder.mGeo.setText(user.getAddress().getGeo().getLat() + " " + user.getAddress().getGeo().getLng());

    }

    @Override
    public int getItemCount() {
        return userList.size();

    }

    public void setFilter(List<User> userModels) {
        userList = new ArrayList<>(userModels);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        public TextView mFirstName;
        public TextView mLastName;
        public TextView mEmail;
        public TextView mId;
        public TextView mCompany;
        public TextView mUserName;
        public TextView mAddress;
        public TextView mWebsite;
        public TextView mGeo;
        public TextView mPhone;
        private CardView mCardView;


        public ViewHolder(View view) {
            super(view);
            mCardView = (CardView) view.findViewById(R.id.cardView);
            mFirstName = (TextView) view.findViewById(R.id.tv_fname);
            mLastName = (TextView) view.findViewById(R.id.tv_lname);
            mEmail = (TextView) view.findViewById(R.id.tv_email);
            mId = (TextView) view.findViewById(R.id.tv_id);
            mCompany = (TextView) view.findViewById(R.id.tv_company);
            mUserName = (TextView) view.findViewById(R.id.tv_userName);
            mAddress = (TextView) view.findViewById(R.id.tv_address);
            mWebsite = (TextView) view.findViewById(R.id.tv_website);
            mGeo = (TextView) view.findViewById(R.id.tv_geo);
            mPhone = (TextView) view.findViewById(R.id.tv_phone);

            view.setOnClickListener(this);
            view.setOnLongClickListener(this);


        }


        @Override
        public void onClick(View v) {

            mCardView.setBackgroundColor(Color.parseColor("#8d8a8b")); //For setting the color of the row, on click
            String id = this.mId.getText().toString();
            String userName = this.mUserName.getText().toString();
            String address = this.mAddress.getText().toString();
            String company = this.mCompany.getText().toString();
            String website = this.mWebsite.getText().toString();
            String phone = this.mPhone.getText().toString();
            String geo = this.mGeo.getText().toString();
            String name = this.mFirstName.getText().toString() + " " + this.mLastName.getText().toString();

            Fragment mUserDetailsFragment = new UserDetailsFragment(); //Get Fragment Instance

            Bundle data = new Bundle();

            data.putString("id", id);
            data.putString("userName", userName);
            data.putString("address", address);
            data.putString("company", company);
            data.putString("website", website);
            data.putString("phone", phone);
            data.putString("geo", geo);
            data.putString("name", name);

            mUserDetailsFragment.setArguments(data);

            MainActivity activity = (MainActivity) context;

            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.recyclerFragment, mUserDetailsFragment, mUserDetailsFragment.toString());
            ft.addToBackStack(null);
            ft.commit();
        }

        @Override
        public boolean onLongClick(View view) {

            return false;
        }
    }

}
