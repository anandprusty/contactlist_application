package com.example.anand.testapplication;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

public class UserDetailsFragment extends Fragment {

    private View mView;
    private TextView mUserName;
    private TextView mPhone;
    private TextView mAddress;
    private TextView mWebsite;
    private TextView mCompany;
    private TextView mGeo;

    private Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
        setHasOptionsMenu(true);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.menu_search).setVisible(false);
        menu.findItem(R.id.menu_ascending).setVisible(false);
        menu.findItem(R.id.menu_descending).setVisible(false);
        menu.findItem(R.id.menu_delete).setVisible(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.user_details_fragment_layout, container, false);

        View view = getActivity().findViewById(R.id.toolbar); //For getting reference of include toolbar in activity_main.xml

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);


        mUserName = (TextView) mView.findViewById(R.id.userNameValue);
        mPhone = (TextView) mView.findViewById(R.id.phoneValue);
        mAddress = (TextView) mView.findViewById(R.id.addressValue);
        mWebsite = (TextView) mView.findViewById(R.id.websiteValue);
        mCompany = (TextView) mView.findViewById(R.id.companyValue);
        mGeo = (TextView) mView.findViewById(R.id.geoValue);

        //Get Argument that passed from activity in "data" key value
        String id = getArguments().getString("id");
        String userName = getArguments().getString("userName");
        String address = getArguments().getString("address");
        String company = getArguments().getString("company");
        String website = getArguments().getString("website");
        String phone = getArguments().getString("phone");
        String geo = getArguments().getString("geo");
        String name = getArguments().getString("name");


        mUserName.setText(userName);
        mAddress.setText(address);
        mCompany.setText(company);
        mWebsite.setText(website);
        mPhone.setText(phone);
        mGeo.setText(geo);


        toolbar.setTitle(name);

        //placing toolbar in place of actionbar
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);


        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(getResources().getDrawable(R.mipmap.ic_back));


        return mView;
    }

}
