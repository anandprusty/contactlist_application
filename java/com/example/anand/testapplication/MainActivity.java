package com.example.anand.testapplication;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;

import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private  FragmentManager mFragmentManager;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View mView = findViewById(R.id.toolbar); //For getting reference of include toolbar in activity_main.xml
        //getting the toolbar
        mToolbar = (Toolbar) mView.findViewById(R.id.toolbar); //For getting the actual toolbar in toolbar.xml

        //setting the title
        mToolbar.setTitle(getResources().getString(R.string.app_name));

        //placing toolbar in place of actionbar
        setSupportActionBar(mToolbar);

        mToolbar.setTitleTextColor(Color.WHITE);


        mFragmentManager = getSupportFragmentManager();
        Fragment fragment = mFragmentManager.findFragmentById(R.id.recyclerFragment);
        if (fragment == null) {
            fragment = new RecyclerFragment();
            mFragmentManager.beginTransaction()
                    .add(R.id.recyclerFragment, fragment)
                    .commit();
        }

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        mToolbar.setTitle("Contacts");
        mToolbar.setNavigationIcon(null);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mToolbar.setTitle("Contacts");
        mToolbar.setNavigationIcon(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);

        return false;
    }

}
